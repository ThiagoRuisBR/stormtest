from flask import Flask, jsonify, request, Response
from operator import itemgetter, attrgetter

app = Flask('SortingService')

class Book:
    def __init__(self, title, author, edition):
        self.title = title
        self.author = author
        self.edition = edition
    
    def __repr__(self):
        return repr((self.title, self.author, self.edition))
    
    def serialize(self):
        return {
            'Title': self.title,
            'Author': self.author,
            'Edition': self.edition
        }
        
books = [
    Book('Java How to Program', 'Deitel & Deitel', 2007),
    Book('Patterns fo Enterprise Application Architecture', 'Martin Fowler', 2002),
    Book('Head First Design Patterns', 'Elisabeth Freeman', 2004),
    Book('Internet & World Wide Web: How to Program', 'Deitel & Deitel', 2007)
]

def validParameters(orderObject):
    if 'column' in orderObject and 'order' in orderObject:
        return True
    else:
        return False

# Default route
@app.route('/books')
def getBooks():
    returnedBooks = [book.serialize() for book in books]
    return jsonify({'books': returnedBooks})


@app.route('/books', methods=['POST'])
def getBooksSorted():
    try:
        requestParameters = request.get_json()
        if len(requestParameters) == 0:
            return jsonify([])
        
        returnedBooks = books
        for index in range(len(requestParameters)-1, -1, -1):
            param = requestParameters[index]
            if(validParameters(param)): 
                if param['order'] == 'DESC':
                    returnedBooks = sorted(returnedBooks, key=attrgetter(param['column']), reverse=True)
                else:
                    returnedBooks = sorted(returnedBooks, key=attrgetter(param['column']))
            
        

        returnedBooks = [book.serialize() for book in returnedBooks]
        return jsonify({'books': returnedBooks})
    except Exception :
        return jsonify({'error': 'SortingServiceException'})



app.run(port=5000)