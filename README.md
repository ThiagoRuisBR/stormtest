# SortingService 

## Descrição
Serviço de ordenação de livros escrito em python utilizando Flask

## Dependências
- Python 3.6.5
- Pip 9.0.1
- Flask 

## Instalação
### Ubuntu
Rodar os seguintes comandos
    
    apt install python3-pip
    pip install Flask

## Execução
Na raiz do projeto rodar o seguinte comando
    
    python3 app.py

O projeto estará rodando em localhost:5000

### Exemplo de chamada
    Endereço: localhost:5000/books
    Método: POST
    Header: Content-Type    application/json
    Body: [{"column":"edition", "order": "DESC"},{"column":"author", "order": "DESC"},{"column":"title", "order": "DESC"}]

Para cada uma das colunas que se deseja ordenar deve-se adicionar um objeto ao array do body da chamada nesse modelo 
   
    {
        "column":"author/edition/title",
        "order": "DESC/ASC"
    }